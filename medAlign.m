%MEDALIGN
%   Function to register/realign calcium imaging data based
%   on control point determination by cross correlation.
%   
%   Based on Daniel Castaño-Díez medianRegister.m script
%
%   Copyright (C) 2016 Friedrich Kretschmer
%   Max Planck Institute for Brain Research
%   friedrich.kretschmer@brain.mpg.de
%
%   [RS, CS] = MEDALIGN(ALLIM, NSUBIM, NPEAKS, LOCALTHRESHOLD);
%
%   ALLIM, the imagestack to be alligned
%   NSUBIM, the number of images to be computed in one iteration 
%       higher is better but needs to be adjusted to available memory 
%       (RAM or GPU)
%   NPEAKS, the number of peaks in the cross correlation 
%       to use for allignement (Daniel used 50)
%   THRESHOLD, above which CC maxima need to lie. Use -inf if you want to
%       include all values
%
%   GPU computation can be activated by setting GPU=TRUE in the
%   first line of the function
%
function [rs, cs] = medAlign(allIm, nSubIm, nPeaks, localThreshold)
    %run this function on gpu
    gpu = false;

    nImages = size(allIm,3);
    nRows = size(allIm,1);
    nCols = size(allIm,2);
    
    rs = nan(nImages,1);
    cs = nan(nImages,1);

    %Calculate template
    if gpu
        tmp = gpuArray(allIm);
        template = gpuArray(median(tmp,3));  
        clear tmp
    else
        template = median(allIm,3);
    end
    template = template-mean(template(:));

    %Calculate crosscorrelation portion for template
    v2_square = repmat((template).^2,1,1,nSubIm);
    F = repmat(fft2(template),1,1,nSubIm);
    clear template templateMat

    for imIdx = 1:nSubIm:nImages
        %Get until end if subset is > nImages when grabbing last chunk
        %Instead of resizing v2 and F one could change indexing below
        %but in this way the code below stays more clear
        if imIdx+nSubIm-1 > nImages
            nSubIm = nSubIm+(nImages-(imIdx+nSubIm-1));
            v2_square = v2_square(:,:,1:nSubIm);
            F = F(:,:,1:nSubIm);
        end

        if gpu
            imMat = gpuArray(allIm(:,:,imIdx:imIdx+nSubIm-1));
        else
            imMat = double(allIm(:,:,imIdx:imIdx+nSubIm-1));
        end

        %Substract mean intensity from each image
        meanMat = mean(mean(imMat));
        meansMat = repmat(meanMat,size(imMat,1),size(imMat,2));
        clear meanMat
        imMat = imMat-meansMat;
        clear meansMat

        %Calculate cross correlation
        M = fft2(imMat);
        
        v1_square = (imMat).^2;
        clear imMat
        tmp  = sqrt(sum(sum(v1_square,1),2) .* sum(sum(v2_square,1),2));
        clear v1_square
        denominator = repmat(tmp,nRows,nCols);
        clear tmp
        numerator = real(ifft2((M).*conj(F)));
        clear M
        cc = numerator./denominator;
        clear numerator denominator
        % Unfortunately imregionalmax is only 2D when ran on GPU
        if gpu
            BW = gpuArray(false(nRows,nCols,nSubIm));
        end
        for i = 1:nSubIm
            BW(:,:,i) = imregionalmax(cc(:,:,i));
        end

        % Get intensities at maxima
        intensityMap = cc .* BW;
        clear cc BW

        % Discard maxima whose value is too low
        intensityMap(intensityMap<localThreshold) = 0;
       
        %put all values from each frame in one column (for sorting)
        tmp = reshape(intensityMap, nRows*nCols, nSubIm);
        clear intensityMap

        %Sort all columns at the same time
        [~,I] = sort(tmp, 'descend');
        clear tmp
       
        %Only keep N brightest spots
        I = I(1:nPeaks,:);
        
        % rMat and cMat contain the rows and cols
        rMat = rem(I-1,nRows) + 1; %more efficient ind2sub
        cMat =  (I-rMat)/nRows + 1; %more efficient ind2sub
        
        % rsMat and csMat contain the shifts in both directions
        if gpu
            rsMat = gpuArray(rMat);
            csMat = gpuArray(cMat);
        else
            rsMat = rMat;
            csMat = cMat;
        end

        for k =1:length(I(:,1))
            [r,c] = ind2sub([nRows,nCols],I(k,1));
            p(k,:) = [r,c];
        end
        
        % Calculate shifts
        rsMat(rMat>nRows/2) = (rMat(rMat>nRows/2) - nRows);
        rsMat(rMat<-nRows/2) = (rMat(rMat<-nRows/2) + nRows);
        clear rMat
        csMat(cMat>nCols/2) = (cMat(cMat>nCols/2) - nCols);
        csMat(cMat<-nCols/2) = (cMat(cMat<-nCols/2) + nCols);
        clear cMat
        
        rsMat = rsMat-1;
        csMat = csMat-1;
        
        %Shift in y direction
        rsMat = round(-rsMat);

        %Shift in x direction
        csMat = round(-csMat);

        %Calculate mean shift and gather from gpu if necessary
        if gpu
            %rs(imIdx:imIdx+nSubIm-1) = gather(mean(rsMat));
            %cs(imIdx:imIdx+nSubIm-1) = gather(mean(csMat));
            rs(imIdx:imIdx+nSubIm-1) = gather(rsMat(1,:));
            cs(imIdx:imIdx+nSubIm-1) = gather(csMat(1,:));
        else
            %rs(imIdx:imIdx+nSubIm-1) = mean(rsMat);
            %cs(imIdx:imIdx+nSubIm-1) = mean(csMat);
            rs(imIdx:imIdx+nSubIm-1) = rsMat(1,:);
            cs(imIdx:imIdx+nSubIm-1) = csMat(1,:);
        end
        clear rsMat csMat
    end
    clear v2_square F
end