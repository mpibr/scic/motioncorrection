function A = binning(A,bin,fun_str)
%
% A=binning(A,bin,fun)
%
% This function rearranges the array A according to the specifications
% of the vector bin. In the first dimension of the generated temporary 
% array the bin elements from all dimension are arranged. By applying 
% functions like 'sum', 'mean' or 'std' to the first dimension, a real
% binning ('sum'), a averaging ('mean') and error calculations can be
% done. 
%
% A=binning(A,bin) The input array 'A' is binned by the vector 'bin'.
% If 'bin' is a scalar, 'bin' elements in each dimenion of 'A' are
% summarized.
%
% Note: The input array 'A' is cut to the maxial integer factor of
% 'bin'. 
%
% A=binning(A,bin,fun) The function 'fun' is applied to the first
% dimension of the rearranged array. Beside the real binning with the
% function 'sum' a number of additional functions can be applied. Up
% to now the following functions are implemented:
% 'sum', 'mean', 'prod'
% 
% Special comment:
% The functionality can be easily extended by implementing additional
% functions, or due to the time consuming permutations by the
% combination of appropriate functions like 'mean' and 'std'.
%
% Author: A. Zeug (last modified 19.07.2007)
% Version: 2.0.0

% comments to: azeug@gwdg.de

tic; t1=toc;
%% Input validation
if nargin < 3
  fun_str='sum';
  fun_type = 1;
elseif sum(strcmp(fun_str,{'sum';'mean';'prod'}))>0
  fun_type = 1;
elseif sum(strcmp(fun_str,{'std';'var'}))>0
  fun_type = 2;
else
  error('Function not implemented yet')
end
fun=str2func(fun_str); % defines function type

if nargin < 1
  help binning
  return
elseif nargin < 2
  error('Please specify binning vector.')
end

%% bin vector dimension
trans = false;
dims  = ndims(A);
if isvector(A)
  dims = 2;
  if size(A,2)==1; trans = true; end
end
if length(bin)==1; 
  bin = bin * ones(1, dims); 
elseif length(bin)~=dims;
  error('Wrong length of binning vector.')
end

%% resizing of A to fit bin
sz=size(A);
if dims==1; sz=length(A); end
bin=min([sz;bin]);
for i=1:dims
  index{i}=1:(bin(i)*floor(sz(i)/bin(i)));
end
A = A(index{:});

%% reshaping A
sz = size(A); if dims==1; sz = length(A); end
v = []; sz_new = sz./bin;
for i=1:dims
  v=[v, bin(i), sz_new(i)];
end
A=reshape(A,v);

%% Calculate binning
if fun_type==1
  % standard type 'sum', 'mean','prod' 
  for i=1:dims
      if isequal(fun, @sum)
          A=fun(A,(2*i-1),'native');
      else
          A=fun(A,(2*i-1));
      end
  end
else
  % type 'std', 'var'
  % need to reshape again to calc function at ones
  v = [];
  for i=1:dims
    v=[v, i, dims+i];
  end
  A = permute(A, v);
  A = reshape(A, [prod(bin) sz_new]);
  try
    A = fun( double(A) );
  catch
    t=toc; fprintf('(%07.3fs) binning: direct calculation failed, start subsequential method (%0.5fs)\n',t,t-t1); t1=t;
    try
      A1 = zeros(sz_new,'double');
      fun1 = @double;
    catch
      t=toc; fprintf('(%07.3fs) binning: calculating %s with double precession failed, try single (%0.5fs)\n',t,func2str(fun),t-t1); t1=t;
      A1 = zeros(sz_new, 'single');
      fun1 = @single;
    end
    h = waitbar(0,'Please wait...','Name','binning: Plese Wait!');
    % subsequential calculation
    sub_dim = find(sz_new == max(sz_new));
    for i = 1:dims; cc{i}=':'; end
    step = floor(1E6 / (prod(sz_new(sz_new < max(sz_new)))*prod(bin)));
    for ii = 0:step:sz_new(sub_dim) % subsequentially...
      waitbar(ii/sz_new(sub_dim),h,['Calculated binning: ' num2str(100*ii/sz_new(sub_dim),'%0.2f') '%']);
      ii_end = min(sz_new(sub_dim), ii+step) - ii;
      cc{sub_dim}=(1:ii_end)+ii;
      A1(cc{:}) = fun(fun1(A(:,cc{:})));
    end
    A = A1;
    delete(h); drawnow;
    t=toc; fprintf('(%07.3fs) binning: calculation with %s precession susseed (%0.5fs)\n',t,func2str(fun1),t-t1); t1=t;
  end
end

%
%A = reshape(A, sz_new);

if dims==1 
  A=squeeze(A);
  if trans==true
    A=A';
  end
else
  A = reshape(A, sz_new);

end

  

 