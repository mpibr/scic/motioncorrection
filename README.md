CUDA compatible Motion correction for Calcium imaging data based on control point determination by cross correlation.

medAlign.m written by Friedrich Kretschmer (based on an initial implementation by Daniel Castaño-Díez)

Contains saveAsTiff.m by YoonOh Tak
Multipage TIFF stack (https://www.mathworks.com/matlabcentral/fileexchange/35684-multipage-tiff-stack), MATLAB Central File Exchange. Retrieved June 15, 2020. 

binning.m by André Zeug



