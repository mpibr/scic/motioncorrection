function data = readTiffNoStack(filename)
t_g = Tiff(filename, 'r');
data = t_g.read();
end